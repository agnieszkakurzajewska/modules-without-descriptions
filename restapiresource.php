<?php
/*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class restapiresource extends Module
{
    /**
     * URL
     * http://root_domain.pl/module/restapiresource/display
     */
    
    public function __construct()
    {
        $this->name = 'restapiresource';
        $this->author = 'Przemek Wleklik';
        $this->tab = 'administration';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Rest Api Additional resources');
        $this->description = $this->l('author: Przemek Wleklik. Module that takes data about products from database and generates XML file (we cannot receive the file)');

        $this->confirmUninstall = $this->l('Are you sure?');
    }

    public function install()
    {

        if(!parent::install()) {
            return false;
        }
        return true;
    }
    

    public function uninstall()
    {
        if(!parent::uninstall()) {
            return false;
        }
        return true;
    }

}